import model.*;
import threads.DownloadThread;
import utils.Bencoder2;
import utils.TorrentInfo;
import utils.Utils;

import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by roberto on 12/7/2015.
 */
public class testconnection {

    public static void main(String[] args) throws Exception {
        // Init torrent
        TorrentInfo torrent = null;
        String torrentFile = args[0];
        String amount_left;
        int piece_size;
        int total_file_size;
        String localID = "abcde12345abcde12345";
        String amount_uploaded = "0";
        String amount_downloaded = "0";

        try {
            torrent = new TorrentInfo(Files.readAllBytes(Paths.get(torrentFile)));
        } catch (Exception e) {
            throw new RuntimeException("Can't read file torrent file : " + torrentFile);
        }
        byte[] file_hash = torrent.info_hash.array();

        // Total amount left
        amount_left = "" + torrent.file_length;

        // Piece size taken from torrent file
        piece_size = torrent.piece_length;

        // Total file size
        total_file_size = torrent.file_length;

        // Number of pieces
        int numberOfPieces = total_file_size / piece_size + 1;

        // Init tracker parameters
        HashMap<String, String> parameters = new HashMap<String, String>();

        // Parameters values
        String[] values = new String[]{
                Utils.toHex(torrent.info_hash.array()),         // SHA1 Hash (in HEX and URL encoded)
                localID,                                        // Peer ID of this client
                "6881",
                amount_uploaded,
                amount_downloaded,
                amount_left,
                "start"
        };

        // Add key-value pairs to parameters hashmap
        for (int i = 0; i < values.length; i++)
            parameters.put(TrackerCommunicator.PARAMETER_KEYS[i], values[i]);

        // Init tracker
        TrackerCommunicator tracker = new TrackerCommunicator(torrent.announce_url.toString(), parameters);


        // Announce download is starting and get list of peers
        Map<ByteBuffer,Object> peers_dictionary = (Map<ByteBuffer,Object>) Bencoder2.decode(tracker.announceStarted(amount_left));
        List<TrackerCommunicator.Peer> peers = tracker.getListOfPeers(peers_dictionary);

        System.out.println(peers);

        // Get test peer
        String test_ip;
        try {
            test_ip = args[1];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            test_ip = "172.31.91.243";
        }
        TrackerCommunicator.Peer test_peer = null;
        for (TrackerCommunicator.Peer peer : peers) {
            if (peer.ip.equals(test_ip)) {
                test_peer = peer;
                break;
            }
        }

        if (test_peer == null) {
            System.out.println("Something's wrong with the test peer " + test_ip);
            return;
        }




        // Connect to it
        PeerCommunicator peer_communication = new PeerCommunicator(localID, test_peer.id, test_peer.ip, test_peer.port);
        peer_communication.piece_size = piece_size;

        BitSet testLocalBitset = new BitSet(numberOfPieces);
        testLocalBitset.set(10);
        testLocalBitset.set(89);
        peer_communication.setLocalBitfield(testLocalBitset);

        SynchronousListOfPieces toDownload = new SynchronousListOfPieces(new LinkedList<Piece>());
        SynchronousListOfPieces downloaded = new SynchronousListOfPieces(new LinkedList<Piece>());
        toDownload.add(new Piece(1, 1));

        DownloadThread thread =
                new DownloadThread(
                        peer_communication,
                        toDownload, downloaded, null, file_hash, numberOfPieces, testLocalBitset);

        Thread theThread = new Thread(thread);
        theThread.start();
        theThread.join();


/*        // Start handshake
        peer_communication.initiateHandshake(file_hash);

        BitSet testLocalBitset = new BitSet(numberOfPieces);
        testLocalBitset.set(10);

        // If EOF, start over sending bitfield in the alternate way
        try {
            /*  Exchange bitfields
            peer_communication.startBitfields(numberOfPieces);

            // Send bitfield to peer
            peer_communication.setLocalBitfield(testLocalBitset);
            peer_communication.sendBitfield(peer_communication.getLocalBitfield(), numberOfPieces);
            System.out.println("Send bitfield to remote");

            // Receive bitfield from peer
            Message bitfieldMessage = peer_communication.receiveMessage();
            peer_communication.setRemoteBitfield(bitfieldMessage);
            BitSet remoteBitfield = peer_communication.getRemoteBitfield();
            System.out.println("remote bitfield: " + Utils.bitSetToString(remoteBitfield));

            // Send interested message
            peer_communication.interested();
            System.out.println("Send interested");

            Message unchokeMessage = peer_communication.receiveMessage();
            MsgType type = MsgType.getID(unchokeMessage.getType());
            System.out.println("Remote send a message with ID: " + type.id());
        }
        catch (EOFException e) {
            System.out.println("\n Catched EOF, sending alternate bitfield \n");

            // Re-start connection
            if (!peer_communication.restartConnection()) return;

            // Start handshake
            peer_communication.initiateHandshake(file_hash);

            /*  Exchange bitfields
            peer_communication.startBitfields(numberOfPieces);

            // Alternate way of sending local bitfield
            peer_communication.setLocalBitfield(testLocalBitset);
            peer_communication.sendBitfieldAlternate(numberOfPieces);

            // Receive bitfield from peer
            Message message = peer_communication.receiveMessage();
            MsgType type = MsgType.getID(message.getType());
            if (type == MsgType.BITFIELD) {
                peer_communication.setRemoteBitfield(message);
                BitSet remoteBitfield = peer_communication.getRemoteBitfield();
                System.out.println("remote bitfield: " + Utils.bitSetToString(remoteBitfield));
            }

            else if (type == MsgType.INTERESTED) {
                peer_communication.setRemoteInterested(true);

                // Send unchoked
                peer_communication.unchoke();
                System.out.println("Remote is interested and unchoked");
            }

            // Send interested message
            peer_communication.interested();
            System.out.println("Send interested");

            message = peer_communication.receiveMessage();
            type = MsgType.getID(message.getType());
            System.out.println("Remote send a message with ID: " + type.id());
        }*/


        System.out.println("Reached end");
        tracker.announceStopped(amount_left);
    }
}
