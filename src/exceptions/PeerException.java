package exceptions;

/**
 * Created by nadiachepurko on 9/26/15.
 */
public class PeerException extends Exception {

    public PeerException(String message) {
        super("Error from a Peer: " + message);
    }
}
