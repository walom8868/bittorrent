import exceptions.TrackerCommunicatorException;
import model.*;
import utils.*;

import java.io.File;
import java.io.IOException;
import java.nio.channels.ShutdownChannelGroupException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Random;

import static utils.Utils.printlnLog;

public class RUBTClient {


	// The peer id of this client
	private static String peer_id;

	// The ports this client might use (range from lowest to highest)
	private static final String LOWEST_PORT_TO_USE = "6881";
	private static final String HIGHES_PORT_TO_USE = "6889";

	// Port to use
	private static final int PORT = 6881;

	// How much this client has uploaded or downloaded to/from another peer
	private static String amount_uploaded = "0";
	private static String amount_downloaded = "0";

	// Amount left to download
	private static String amount_left = "0";

	// Event to report to tracker (default value is started)
	private static String event = "started";

	public static void main(String[] args) throws Exception {
   try{
		if(args.length != 2){
			printlnLog("Please, provide 2 arguments: torrent file and the file to save the data to");
			return;
		}

		String torrentFile = args[0];
		String toFile = args[1];

		String clientID = generateClientID();

		// Start peer server for incoming connections
		new PeerServer(clientID, PORT).startListener();
		Thread.sleep(1000);

      	DownloadManager dm = new DownloadManager(generateClientID(), torrentFile, toFile);

      	dm.startDownload();

   } catch (Exception e) {
     e.printStackTrace();
   }
  }


	/* helper methods for testing */
	public static void printTorrentInfo(TorrentInfo torrent, String hash_hex){
		System.out.println("\n-------------- Operations with torrent file ----------\n");
		System.out.println(
				"URL of the tracker: " + torrent.announce_url.toString()
						+ "\nPiece length: " + torrent.piece_length
						+ "\nFile name: " + torrent.file_name
						+ "\nFile length: " + torrent.file_length
						+ "\npiece_hashes array length " + torrent.piece_hashes.length
						+ "\nSHA-1 hash value (in hex): " + hash_hex);
	}

	// Generate a random 20-byte string to use as ID
	// The result is an alphabetic string for simplicity
	public static String generateClientID() {
		String id = "Flinner";
		int id_length = 13;
		Random randomizer = new Random();

		for (int i = 0; i < id_length; i++)
			id += (char) (randomizer.nextInt(25) + 97);      // 97 is 'a' is ASCII

		return id;
	}
}
