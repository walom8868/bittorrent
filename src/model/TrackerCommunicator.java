package model;

import exceptions.TrackerCommunicatorException;
import utils.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class TrackerCommunicator {

	// Simple peer class to encapsulate the properties of each retrieved peer
	public static class Peer {
		public String ip;
		public int port;
		public byte[] id;

		public Peer(byte[] id, String ip, int port) {
			this.ip = ip;
			this.port = port;
			this.id = id;
		}

		@Override
		public String toString() {
			return "ID: " + new String(id) + "\nIP: " + ip + "\nPort: " + port + "\n\n";
		}
	}

	public final static ByteBuffer INTERVAL_KEY = ByteBuffer.wrap(new byte[]
			{ 'i', 'n', 't', 'e', 'r', 'v', 'a', 'l' });

	public final static ByteBuffer MIN_INTERVAL_KEY = ByteBuffer.wrap(new byte[]
			{ 'm', 'i', 'n', ' ', 'i', 'n', 't', 'e', 'r', 'v', 'a', 'l' });

	public final static ByteBuffer PEERS_KEY = ByteBuffer.wrap(new byte[]
			{ 'p', 'e', 'e', 'r', 's' });

	public final static ByteBuffer PEER_ID_KEY = ByteBuffer.wrap(new byte[]
			{ 'p', 'e', 'e', 'r', ' ', 'i', 'd' });

	public final static ByteBuffer IP_KEY = ByteBuffer.wrap(new byte[]
			{ 'i', 'p' });

	public final static ByteBuffer PORT_KEY = ByteBuffer.wrap(new byte[]
			{ 'p', 'o', 'r', 't' });

	public static final String[] PARAMETER_KEYS = {"info_hash", "peer_id", "port", "uploaded", "downloaded", "left", "event"};

	private String baseURL;

	private Map<String, String> parameters = null;

	private List<Peer> peers;
	private int interval;
	private int min_interval;

	public TrackerCommunicator(String baseURL, String parameters) throws MalformedURLException {
		this.baseURL = baseURL + "?" +  parameters;
	}

	public TrackerCommunicator(String baseURL, Map<String, String> parameters)
			throws MalformedURLException, TrackerCommunicatorException, UnsupportedEncodingException {

		this.parameters = parameters;
		this.baseURL = baseURL;
	}

	/* HTTP GET request to a tracker. Returns response dictionary with information about peers
	Dictionary consist of the following keys:
	'failure reason' optional, 'interval', 'complete' optional, 'incomplete' optional, 'peers' (: peer id, ip, port)
	*/
	public byte[] get() throws Exception {
   try{
   		URL url;

   		if (!this.baseURL.contains("?")) {
		// Build the url
		String buffer = this.baseURL + "?";

		// Get only the parameters used in an HTTP GET request for the tracker
		// Any other key-value pairs passed will be ignored
		for (String key : PARAMETER_KEYS) {
			// If one of these returns null, throw exception (except for event)
			if (this.parameters.get(key) == null)
				throw new TrackerCommunicatorException("One of the required GET parameters is NULL");

			buffer += key + "=" + this.parameters.get(key) + "&";
		}

		// Remove the trailing '&'
		buffer = buffer.replaceAll("&?$", "");

			url = new URL(buffer);
		}
		else {
			url = new URL(this.baseURL);
		}
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// optional default is GET
		connection.setRequestMethod("GET");

		int responseCode = connection.getResponseCode();
		//System.out.println("\nSending 'GET' request to URL : " + url);

		// Read and keep response in bytes
		byte[] response = getByteArray(connection);

		return response;
   } catch(SocketException e) {
    throw new TrackerCommunicatorException("Tracker connection error => " + e.getMessage());
   }
	}

    // Get list of peers from a dictionary response from the tracker
    public List<Peer> getListOfPeers(Map<ByteBuffer,Object> dictionary) throws Exception {
        // Get the list of peers (dictionaries)
        List<Map> peers_list = (List<Map>) dictionary.get(PEERS_KEY);

        interval = Integer.parseInt(dictionary.get(INTERVAL_KEY).toString());
		min_interval = Integer.parseInt(dictionary.get(MIN_INTERVAL_KEY).toString());

        // Decode dictionaries and print info
        peers = new ArrayList<Peer>();
        Iterator<Map> peers_iterator = peers_list.iterator();
        for (int i = 0; peers_iterator.hasNext(); i++) {

            Map<ByteBuffer, Object> peer_dictionary = peers_iterator.next();

            //String peer_id = new String(((ByteBuffer) peer_dictionary.get(PEER_ID_KEY)).array(), "ASCII");
            byte[] peer_id = ((ByteBuffer) peer_dictionary.get(PEER_ID_KEY)).array();
            String peer_ip = new String(((ByteBuffer) peer_dictionary.get(IP_KEY)).array(), "ASCII");
            Integer peer_port = (Integer)peer_dictionary.get(PORT_KEY);

            addPeer(peer_id, peer_ip, peer_port);
        }

        return peers;
    }

    // Communicate to tracker that this download has just started
    // Left is the amount of bytes left to download
    public byte[] announceStarted(String left) throws Exception {
   		setParameter("event", "started");
		setParameter("left", left);
		byte[] response =  get();

		// Get the intervals returned from the tracker
		Map<ByteBuffer,Object> decoded_response = (Map<ByteBuffer,Object>) Bencoder2.decode(response);
		//System.out.println(decoded_response);

		return response;
    }

    // Communicate to tracker that download is complete
    public void announceCompleted() throws Exception {
    	setParameter("event", "completed");
		setParameter("left", "0");
		get();
    }

	// Communicate  to tracker that download has stopped
	public void announceStopped(String left) throws Exception {
		setParameter("event", "stopped");
		setParameter("left", left);
		get();
	}

	// Update status to tracker
	public void update(String left) throws Exception {
		// Event is not needed here
		if (this.parameters.containsKey("event"))
			this.parameters.remove("event");
		setParameter("left", left);

		// Wait for the interval to send next update


		get();
	}

	/* add only specific peers to the list of peers */
	/* for Phase1 we must use peer with -RU prefix*/
	private void addPeer(byte[] peer_id, String peer_ip, Integer peer_port) {
		//if(new String(peer_id).contains("-RU") && peer_ip.contains("128.6.171.132")){
		//if(new String(peer_id).contains("-RU") || peer_ip.contains("128.6.171.130")) {
		//if (peer_ip.contains("128.6.171.130")) {
			Peer peer = new Peer(peer_id, peer_ip, peer_port);
			peers.add(peer);
		//}
	}

	public static byte[] getByteArray(HttpURLConnection connection) throws IOException{

		InputStream is = connection.getInputStream();
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];  	//TODO: Is it sufficiently enough of memory?

		while ((nRead = is.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}

		buffer.flush();

		return buffer.toByteArray();
	}

	public int getInterval() {
		return interval;
	}

	public List<Peer> getPeers() {
		return peers;
	}

	public void setParameter(String key, String value) {
		parameters.put(key, value);
	}

	public static void main(String[] args) throws Exception {
		TrackerCommunicator tracker = new TrackerCommunicator(
			"http://128.6.171.130:6969/announce",
			"info_hash=%56%D3%D5%C5%4F%A6%38%A5%3F%28%0F%C9%41%10%60%EF%26%2D%FE%B1&peer_id=local54321098765431&port=6881&uploaded=0&downloaded=0&left=14285814&event=started"
		);

		Map<ByteBuffer,Object> peers_dictionary = (Map<ByteBuffer,Object>) Bencoder2.decode(tracker.get());
   
      	List<Peer> peers = tracker.getListOfPeers(peers_dictionary);

      	System.out.println(peers.get(0));
      	System.out.println(peers.get(1));
	}
}


