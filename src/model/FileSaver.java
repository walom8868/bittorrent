package model;

import java.util.*;
import java.io.*;

public class FileSaver {

	private String fileName;
	private int totalNumberOfPieces;
	private SynchronousListOfPieces downloaded;
	private int piece_size;

	private int savedPieces;
	private RandomAccessFile outFile;

	public FileSaver(String fileName, int totalNumberOfPieces, 
			SynchronousListOfPieces downloaded, int piece_size) throws FileNotFoundException {
		this.fileName = fileName;
		this.totalNumberOfPieces = totalNumberOfPieces;
		this.downloaded = downloaded;
		this.piece_size = piece_size;

		this.savedPieces = 0;

		// Open outFile
		outFile = new RandomAccessFile(fileName, "rw");
	}

	// Save all file
	// Pieces are saved in order
	public void saveFile() throws Exception {
		Piece piece = null;

		while (savedPieces < totalNumberOfPieces) {
			System.out.println("Next Piece to save: " + savedPieces);
			// Check we're getting pieces in order
			int i = 0;
			boolean foundNextPiece = false;
			do {
				try {
					piece = downloaded.getIndex(i);
				}
				// This only hits if queue is completely empty
				// Only wait until something is inserted
				catch (IndexOutOfBoundsException e) {
					System.out.println("Waiting 3 seconds for pieces to save.");
					Thread.sleep(3000);
				}
				
				if (piece == null)
					continue;
				
				//System.out.println("Piece found: " + piece.index);

				// If found the right piece, break
				if (piece.getIndex() == savedPieces) {
					foundNextPiece = true;
					System.out.println("Found it");
					break;
				}

				i++;
			}
			while (i < downloaded.size());

			// If piece was not found, wait 1 second and repeat
			if (!foundNextPiece) {
				System.out.println("Not found, waiting 3 second for it");
				Thread.sleep(3000);
				continue;
			}

			// Else, save the piece
			savePiece(piece, piece.index * piece_size);
			downloaded.remove(i);
			savedPieces++;
		}

		// Close the file
		outFile.close();
	}

    // save piece to file in particular position with piece (block) offset
    private void savePiece(Piece piece, int offset) throws IOException {
        outFile.seek(offset);
        outFile.write(piece.getData());
    }


    public static void main(String[] args) throws FileNotFoundException, Exception {
    	Piece piece0 = new Piece(0, new byte[] {'a', 'b', 'c', 'd', 'e'});
    	Piece piece1 = new Piece(1, new byte[] {'0', '1', '2', '3', '4'});
    	final Piece piece2 = new Piece(2, new byte[] {'f', 'g', 'h', 'i', 'j'});
    	Piece piece3 = new Piece(3, new byte[] {'5', '6', '7', '8', '9'});
    	Piece piece4 = new Piece(4, new byte[] {'z', 'y', 'x', 'w', 'v'});

    	final SynchronousListOfPieces list = new SynchronousListOfPieces(new LinkedList<Piece>());

    	list.add(piece0);
    	list.add(piece4);
    	list.add(piece3);
    	list.add(piece1);
    	//list.add(piece2);

    	int numberofpieces = 5;

    	FileSaver fs = new FileSaver("testsave.txt", numberofpieces, list, 5);

    	Runnable delayInput = new Runnable() {
    		@Override
    		public void run() {
    			try {
    				Thread.sleep(3000);
    				list.add(piece2);
    			}
    			catch (Exception e) {
    				System.out.println("bad");
    			}
    		}
    	};

    	(new Thread(delayInput)).start();

    	fs.saveFile();
    }
}