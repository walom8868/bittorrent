package model;

import exceptions.*;
import model.*;
import java.util.*;
import java.util.BitSet;
import utils.*;

import java.security.NoSuchAlgorithmException;

import java.net.*;
import java.io.*;
import java.nio.*;

import static utils.Utils.printLog;
import static utils.Utils.printlnLog;

/**
 *  model.PeerCommunicator.java
 *  Logical representation of the communication between 2 peers (local and remote)
 *  Contains basic communication methods between peers, like handshake and messages
 */


public class PeerCommunicator {

    /** Defaults (Time measured in miliseconds) **/
    private final static int THREAD_SLEEP_INTERVAL = 1000;
    private static final int KEEP_ALIVE_INTERVAL = 120000;

    // FOR TESTING ONLY
    public static final int PIECE_SIZE = 12;


    // Peer IDs
    private String localPeerID, remotePeerID;
    private byte[] remotePeerID_bytes;

    // Connection status
    private boolean localInterested, localChoked,
                    remoteInterested, remoteChoked;

    // Requested file
    private byte[] current_file_hash;
    public int piece_size = PIECE_SIZE;

    // FileManager to refer to
    private FileManager fm;

    // Connection's socket and I\O
    public Socket socket = null;
    private DataOutputStream out;
    public DataInputStream in;

    // For new connections
    public String remoteIP;
    private int   remotePort;

    // Last Keep-Alive time
    private long lastKeepAliveTime;

    // Remote peer's bitfield
    private BitSet remoteBitfield = null;
    private BitSet localBitfield = null;


    /**  Constructors  **/

    public PeerCommunicator(String localPeerID, String remotePeerID, Socket socket) throws Exception {
        this.localPeerID = localPeerID;
        if (remotePeerID != "") this.remotePeerID = remotePeerID;
        this.socket = socket;
        openConnection();
    }

    public PeerCommunicator(String localPeerID, String remotePeerID, String remoteIP, int remotePort) throws Exception {
        this.localPeerID = localPeerID;
        this.remotePeerID = remotePeerID;
        this.remoteIP = remoteIP;
        this.remotePort = remotePort;

        // Open connection automatically
        openConnection();
    }

    public PeerCommunicator(String localPeerID, byte[] remotePeerID, String remoteIP, int remotePort) throws Exception {
        this.localPeerID = localPeerID;
        this.remotePeerID_bytes = remotePeerID;
        this.remoteIP = remoteIP;
        this.remotePort = remotePort;

        // Open connection automatically
        openConnection();     
    }

    /****** Methods to manage socket layer *******/

    public void openConnection() throws Exception {
        // Catch possible exceptions a socket might cause
        // Security exception not needed yet since we don't have a security manager
        // We'll assure proper arguments are passed to this class (assumption)
        if (this.socket == null || this.socket.isClosed()) {
            try {
                this.socket = new Socket(remoteIP, remotePort);
            }
            catch (UnknownHostException e) {
                throw new PeerException("The host "+remoteIP+" is invalid or unreachable");
            }
            catch (IOException e) {
                throw new PeerException("Unable to assign I/O functionality to the socket to "+remoteIP);
            }
        }

        // Set timeout - 30 seconds
        //socket.setSoTimeout(30000);

        // Get the IO streams from the socket
        try {
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
        }
        catch (IOException e) {
            throw new PeerException("Unable to open IO streams for the socket to "+remoteIP);
        }

        // All communications start choked and not interested
        this.remoteChoked = true;
        this.remoteInterested = false;
        this.localChoked = true;
        this.localInterested = false;
    }

    public boolean closeConnection() {
        try {
            in.close();
            out.close();
            socket.close();
        }
        catch (Exception e) {
            System.out.println("Error closing socket and IO streams with remote peer");
            return false;
        }

        return true;
    }

    public boolean restartConnection() {
            closeConnection();
        try {
            openConnection();
        }
        catch (Exception e) {
            System.out.println("Error restarting connection");
            e.printStackTrace();
            return false;
        }
        System.out.println("Connection restarted...");
        return true;
    }


    /***** Methods to check connection's logical status *********/

    public boolean isConnectionAlive() {
        long diffInMillis = System.currentTimeMillis() - lastKeepAliveTime;
        return diffInMillis < KEEP_ALIVE_INTERVAL;
    }

    public boolean amIDownloadingFromPeer() { return localInterested && !localChoked; }

    public boolean amIUploadingToPeer() { return remoteInterested && !remoteChoked; }


    /****** Handshake *******/

    /* perform a handshake with this remote peer */
    public void initiateHandshake(byte[] infoHash) throws Exception {
      try{
        byte[] handshake = MsgUtils.buildHandshake(localPeerID, infoHash);
        sendMessage(handshake);

        byte[] handshakeResponse = new byte[68];
        try {
            in.readFully(handshakeResponse);
        }
        catch (EOFException e) {
            throw new PeerException("The remote peer did not send a valid reply-handshake (bytes are missing)");
        }

        MsgUtils.verifyHandShakeResponse(handshakeResponse, handshake, remotePeerID_bytes);

        // Once handshake succeeded, set the hash
        this.current_file_hash = infoHash;

      } catch (SocketTimeoutException ex) {
            Utils.printLog("####### SocketTimeoutException ######");
            throw new PeerCommunicationException("Connection with peer is not alive!");
      }
    }

    /**
     * Respond to an incoming handshake message and complete
     * @return boolean     True if handshake was successful, else false
     */

    public boolean replyHandshake() throws Exception {
        // Read incoming handshake from remote peer
        byte[] incoming_handshake = new byte[68];
        try {
            in.readFully(incoming_handshake);
        }
        catch (EOFException e) {
            System.out.println("PeerException thrown");
            throw new PeerException("The remote peer did not send a valid initial-handshake (bytes are missing)");
        }

        Utils.printlnLog("Initial Handshake: " + new String(incoming_handshake));
        byte[] file_hash = Arrays.copyOfRange(incoming_handshake, 28, 48);

        // If incoming handshake is not valid, return false
        if (!Arrays.equals(Arrays.copyOfRange(incoming_handshake, 0, 20), MsgUtils.HANDSHAKE_HEAD)    // Verify first 20 bytes
            || !fm.isFileReadyToBeShared(file_hash)) {       // Verify file's hash is available at local
            System.out.println("Validation not passed");
            return false;
        }

        Utils.printlnLog("Handshake was good, sending reply");

        // Handshake was good, so set this remote peer's ID
        this.remotePeerID = new String(Arrays.copyOfRange(incoming_handshake, 48, 68));

        // Send reply handshake
        try {
            sendMessage(MsgUtils.buildHandshake(localPeerID, file_hash));

            // Set hash of requested file
            this.current_file_hash = file_hash;

            // Set the piece size to use
            this.piece_size = this.fm.getPieceSize(Utils.toHex(file_hash));
        }
        catch (Exception e) {
            throw new PeerCommunicationException("Connection with remote peer is dead");
        }

        return true;
    }


    /******* Receive and reply from remote *******/

    public Message receiveMessage() throws IOException, PeerCommunicationException {

        try {
            byte[] bigendianLength = new byte[4];
            in.readFully(bigendianLength);
            int msgLength = MsgUtils.convertToInt(bigendianLength);
            lastKeepAliveTime = System.currentTimeMillis();
            if (msgLength > 0) {
                byte[] msgBody = new byte[msgLength];
                in.readFully(msgBody);
                return MsgUtils.parseMessage(ByteBuffer.wrap(msgBody));
            } else {
                Utils.printLog("####### KeepAlive message received! #######");
            }
        } catch (SocketTimeoutException ex) {
            Utils.printLog("####### SocketTimeoutException ######");
            throw new PeerCommunicationException("Connection with peer is not alive!");
        }
        return null;
    }

    /** Appropiately reply to any incoming message  */
    public void respondMessage(Message receivedMessage) throws Exception {
        System.out.println("Replying to message type: " + MsgType.getID(receivedMessage.getType()).id());
        switch (MsgType.getID(receivedMessage.getType())) {

            // Remote peer choked me
            case CHOKE:
                setLocalChoked(true);
                break;

            // Remote peer unchoked me
            case UNCHOKE:
                setLocalChoked(false);
                break;

            // Remote is interested
            case INTERESTED:
                setRemoteInterested(true);
                break;

            // Remote is not interested
            case UNINTERESTED:
                setRemoteInterested(false);
                break;

            // Remote sent its bitfield
            case BITFIELD:
                // Need to store it, up to the execution thread to send my bitfield
                setRemoteBitfield(receivedMessage);
                break;

            // Remote send a have, need to update my copy of its bitfield
            case HAVE:
                int pieceToUpdate = receivedMessage.getPayload().getInt();
                remoteBitfield.set(pieceToUpdate);
                break;

            // Nothing to reply for a piece message
            case PIECE:
                break;

            // Return the requested piece if available
            case REQUEST:
                Block block = receivedMessage.getBlock();
                System.out.println("piece size: " + piece_size + " - pieceindex: " + block.pieceIndex + " - block offset: " + block.offset);
                System.out.println("BLOCK OFFSET REAL: " + block.getRealOffset(piece_size));
                block.setData(fm.getBlock(current_file_hash, block.getRealOffset(piece_size), block.getLength()));
                sendPiece(receivedMessage.getBlock());
                break;
            default:
                break;
        }
    }

    /** Respond incoming messages **/
    public void respondMessages(FileManager fm) throws Exception {
        this.fm = fm;

        Message received_message;

        // Perform good handshake before anything else
        // Drop connection if handshake failed
        if (!replyHandshake())
            Utils.printlnLog("Connection to incoming peer dropped");

        // Loop and keep replying any messages
        while ((received_message = receiveMessage()) != null) {
            // Print it to screen
            System.out.println(received_message);

            // Decide what to reply
            switch (MsgType.getID(received_message.getType())) {
                // Don't send any reply for these messages
                case CHOKE:
                case UNCHOKE:
                case INTERESTED:
                case UNINTERESTED:
                case BITFIELD:
                case HAVE:
                case PIECE:
                    break;

                // Send the requested piece
                case REQUEST:
                    //byte[] data = fm.getBlock(current_file_hash, offset, length);
                    Block block = received_message.getBlock();
                    System.out.println("piece size: " + piece_size + " - pieceindex: " + block.pieceIndex + " - block offset: " + block.offset);
                    System.out.println("BLOCK OFFSET REAL: " + block.getRealOffset(piece_size));
                    block.setData(fm.getBlock(current_file_hash, block.getRealOffset(piece_size), block.getLength()));
                    sendPiece(received_message.getBlock());
                    break;
                default:
                    break;
            }
        }
    }


    /******* Send to remote *******/

    public void sendMessage(byte[] msgBytes) throws PeerException {
        try {
            this.out.write(msgBytes);
            this.out.flush();
        }
        catch (IOException e) {
            throw new PeerException("Error ocurred while trying to send message from peer" + remotePeerID);
        }
    }

    public void sendKeepAlive() throws Exception {
        this.sendMessage(MsgUtils.buildKeepAlive());
    }

    public void choke() throws Exception { 
        this.sendMessage(MsgUtils.buildChoke());
        this.remoteChoked = true;
    }

    public void unchoke() throws Exception { 
        this.sendMessage(MsgUtils.buildUnChoke());
        this.remoteChoked = false;
    }

    public void interested() throws Exception { 
        this.sendMessage(MsgUtils.buildInterested());
        this.localInterested = true;
    }

    public void uninterested() throws Exception { 
        this.sendMessage(MsgUtils.buildUninterested());
        this.localInterested = false;
    }

    public void sendHave(int piece_index) throws Exception {
        this.sendMessage(MsgUtils.buildHave(piece_index));
    }

    public void sendBitfield(BitSet bitfield, int length) throws IOException, PeerException {
        this.sendMessage(MsgUtils.buildBitfield(bitfield, length));
    }

    public void sendBitfield(int length) throws Exception {
        this.sendMessage(MsgUtils.buildBitfield(this.localBitfield, length));
    }

    public void sendBitfieldAlternate(int length) throws Exception {
        // Build a bunch of have messages for all the available pieces
        for (int i = 0; i < length; i++) {
            if (this.localBitfield.get(i)) {
                sendHave(i);
                System.out.println("Send have for piece# " + i);
            }
        }
    }

    public void sendRequest(Block block) throws Exception {
        // Check that i'm not choked by the peer
        this.sendMessage(MsgUtils.buildRequest(block));
    }

    public void sendRequest(int index, int offset, int length) throws Exception {
        this.sendMessage(MsgUtils.buildRequest(index, offset, length));
    }   

    public void sendPiece(Block block) throws Exception {
        this.sendMessage(MsgUtils.buildPiece(block));
    }

    public void sendPiece(int length, int index, int offset, byte[] data) throws Exception {
        // Retrieve data from the requested file
        this.sendMessage(MsgUtils.buildPiece(length, index, offset, data));
    }

    /**
     *  Getters, Setters and basic overrides
     */

    public void setLocalChoked(boolean localChoked) {
        this.localChoked = localChoked;
    }

    public void setRemoteInterested(boolean remoteInterested) {
        this.remoteInterested = remoteInterested;
    }

    public BitSet getRemoteBitfield() { return this.remoteBitfield; }

    public void setRemoteBitfield(Message bitfield_msg) {
        this.remoteBitfield = BitSet.valueOf(bitfield_msg.getPayload());
    }

    public void startLocalBitfield(int length) {
        this.localBitfield = new BitSet(length);
    }
    public void setLocalBitfield(BitSet bitfield) { this.localBitfield = bitfield; }

    public void startBitfields(int length) {
        this.localBitfield = new BitSet(length);
        this.remoteBitfield = new BitSet(length);
    }

    public BitSet getLocalBitfield() {
        return this.localBitfield;
    }

    public String getRemoteIP() {
        return this.remoteIP;
    }

    @Override
    public String toString() {
        return "Peer{" +
                "id='" + remotePeerID + '\'' +
                ", ip='" + remoteIP + '\'' +
                ", port=" + remotePort +
                ", localInterested=" + localInterested +
                ", localChoked=" + localChoked +
                ", remoteInterested=" + remoteInterested +
                ", remoteChoked=" + remoteChoked +
                '}';
    }
}
