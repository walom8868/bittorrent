package model;
import java.util.*;

public class SynchronousListOfPieces {

	public List<Piece> list;

	public SynchronousListOfPieces(List list) {
		this.list = list;
	}

	public synchronized void add(Piece piece) {
		list.add(piece);
	}

	public synchronized void addToHead(Piece piece) {
		list.add(0, piece);
	}

	public synchronized Piece addToHeadAndGetIndex(Piece piece, int index) {
		list.add(0, piece);
		System.out.println("From list: trying index " + index + " - list size now: " + list.size());
		if (list.size() > index) {
			Piece newPiece = list.get(index);
			list.remove(index);
			return newPiece;
		}
		else
			return null;
	}

	public Piece getIndex(int index) {
		return list.get(index);
	}

	public synchronized void remove(int index) {
		list.remove(index);
	}

	// Retrieve and remove list's head
	public synchronized Piece poll() {
		System.out.println("List size: " + list.size());
		Piece piece;
		int position = 0;
		do {
			piece = list.get(position);
			list.remove(position);
			if (piece == null) {
				System.out.println("========== FROM LIST, POSITION: " + position +" IS NULL");
				position++;
			}
		} while (piece == null);
		return piece;
	}

	// Peek
	public synchronized Piece peek() {
		Piece piece = list.get(0);
		return piece;
	}

	public synchronized int size() {
		return list.size();
	}

	@Override
	public String toString() {
		String s = "";
		ListIterator<Piece> iterator = list.listIterator();
		while (iterator.hasNext())
			s += iterator.next().toString();

		return s;
	}
}