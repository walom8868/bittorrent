import java.util.*;
import java.net.*;
import java.nio.*;
import java.nio.file.*;

import downloadalgorithms.RareFirstDownloadAlgorithm;
import downloadalgorithms.SimpleDownloadAlgorithm;
import threads.DownloadAlgorithmThread;
import threads.DownloadThread;
import threads.FileSaverThread;
import utils.*;
import model.*;

public class DownloadManager {

    private String localID;
    private String torrentName;
    private String fileName;

    private TorrentInfo torrent;
    private TrackerCommunicator tracker;

    // Piece size for this file
    private int piece_size;
    private int total_file_size;

    // Keep track of the status of each piece
    private BitSet bitfield;

    // Queues of pieces
    private SynchronousListOfPieces toDownload;
    private SynchronousListOfPieces downloaded;

    // Arguments to keep track of
    // How much this client has uploaded or downloaded to/from another peer
    private static String amount_uploaded = "0";
    private static String amount_downloaded = "0";

    // Amount left to download
    private static String amount_left = "0";

    public DownloadManager(String localID, String torrentFile, String fileName) throws Exception {
        this.torrentName = torrentFile;
        this.fileName = fileName;
        this.localID = localID;

        initTorrentAndTracker(torrentFile);
    }

    public void initTorrentAndTracker(String torrentFile) throws Exception {
        // Init torrent
        try {
            this.torrent = new TorrentInfo(Files.readAllBytes(Paths.get(torrentFile)));
        } catch (Exception e) {
            throw new RuntimeException("Can't read file torrent file : " + torrentFile);
        }

        // Total amount left 
        this.amount_left = "" + this.torrent.file_length;

        // Piece size taken from torrent file
        this.piece_size = this.torrent.piece_length;
        // Total file size
        this.total_file_size = this.torrent.file_length;

        // Init tracker parameters
        HashMap<String, String> parameters = new HashMap<String, String>();

        // Parameters values
        String[] values = new String[]{
          Utils.toHex(torrent.info_hash.array()),                  // SHA1 Hash (in HEX and URL encoded)
          localID,                            // Peer ID of this client
          "6881",
          amount_uploaded,
          amount_downloaded,
          amount_left,
          "start"
        };

        // Add key-value pairs to parameters hashmap
        for (int i = 0; i < values.length; i++)
          parameters.put(TrackerCommunicator.PARAMETER_KEYS[i], values[i]);

        // Init tracker
        this.tracker = new TrackerCommunicator(torrent.announce_url.toString(), parameters);
    }


    public void startDownload() throws Exception {

        // Announce download is starting and get list of peers
        Map<ByteBuffer,Object> peers_dictionary = (Map<ByteBuffer,Object>) Bencoder2.decode(tracker.announceStarted(amount_left));
        List<TrackerCommunicator.Peer> peers = tracker.getListOfPeers(peers_dictionary);

        BitSet[] all_possible_bitfields = new BitSet[peers.size()];
        // Make a bitset for the peers bitfields
        // When marked to 1, means that peer returned its bitfield
        BitSet bitfieldsFromPeers = new BitSet(peers.size());
        
        System.out.println(peers);

        System.out.println("File_hash: " + new String(this.torrent.info_hash.array()));

        // Initialize lists for pieces to download/downloaded
        this.toDownload = new SynchronousListOfPieces(new LinkedList<Piece>());
        this.downloaded = new SynchronousListOfPieces(new LinkedList<Piece>());

        // Maintain list of threads
        // At index 0 will always be the download algorithm thread, at index 1 the file saver thread
        // All others are peer communication threads 
        List<Thread> threads = new ArrayList<Thread>();

        RareFirstDownloadAlgorithm downloadAlgorithm = new RareFirstDownloadAlgorithm(this.total_file_size / this.piece_size);

        // Initialize algorithm and file saving threads
        // Download algorithm
        DownloadAlgorithmThread da_thread = new DownloadAlgorithmThread(
            downloadAlgorithm, toDownload, this.piece_size, this.total_file_size, torrent.piece_hashes
        );

        // model.FileSaver
        FileSaverThread fs_thread = new FileSaverThread(
            new FileSaver(this.fileName, this.total_file_size / this.piece_size + 1, downloaded, this.piece_size)
        );

        // Add these two threads to the list
        threads.add(new Thread(da_thread));
        threads.add(new Thread(fs_thread));

        // Hold the peer communication objects separately (can't retrieve them from a java-native thread)
        List<DownloadThread> peerComm_threads = new ArrayList<DownloadThread>();

        // Initialize each thread and add it to the list
        for (int i = 0; i < peers.size(); i++) {
            TrackerCommunicator.Peer current_peer = peers.get(i);

            peerComm_threads.add(
                new DownloadThread(
                        localID, current_peer, this.toDownload, this.downloaded,
                        threads.get(0), this.torrent.info_hash.array(),
                        this.total_file_size / this.piece_size + 1, this.piece_size
                )
            );

            threads.add(new Thread(peerComm_threads.get(i)));
        }

        // Start all the threads
        // First the connections with the peers, because they have to handshake
        for (int i = 2; i < threads.size(); i++) {
            threads.get(i).start();
        }

        // Now the download algorithm and filesaver
        threads.get(0).start();
        threads.get(1).start();

        // Try to get the bitfields from the connected peers
        for (int i = 2; i < threads.size(); i++) {
            Thread this_thread = threads.get(i);
            DownloadThread this_download_thread = peerComm_threads.get(i-2);

            // If the thread is still alive, meaning no connection error happened
            while (this_thread.isAlive()) {
                System.out.println("Wait a moment, still trying to connect to all peers to get bitfields");
                if (this_download_thread.peerComm != null && this_download_thread.peerComm.getRemoteBitfield() != null) {
                    all_possible_bitfields[i-2] = this_download_thread.peerComm.getRemoteBitfield();
                    break;
                }
                else {
                    Thread.sleep(1000);
                }
            }
        }

        System.out.println("All bitfields collected");

        // Now give the bitfields to the Algorithm Thread
        downloadAlgorithm.setBitfields(all_possible_bitfields);

        // Wait until file saver thread finishes
        threads.get(1).join();

        System.out.println("Download complete!");
        
        // Announce tracker about it
        tracker.announceCompleted();
    }


    public void markBitfieldDownloaded(int pieceIndex) {
        bitfield.set(Utils.toBitSetIndex(pieceIndex, bitfield));
    }

    public void setBitfield(ByteBuffer bitfield) {
        this.bitfield = BitSet.valueOf(bitfield);
    }

/*
    public static void main(String[] args) throws Exception {
        // Start a peer server
        new PeerServer("remote78901234567890", 6881).startListener();

        String file_hash = "%56%D3%D5%C5%4F%A6%38%A5%3F%28%0F%C9%41%10%60%EF%26%2D%FE%B1";
        //"%A7%D3%D5%C5%4F%A6%38%A5%3F%28%0B%C9%41%10%60%EF%26%2D%FE%B6";
        int port = 25760;//4444;

        Socket socket1 = new Socket("128.6.171.131", port);
        Socket socket2 = new Socket("128.6.171.131", port);
        Socket socket3 = new Socket("128.6.171.131", port);

        SynchronousListOfPieces toDownload = new SynchronousListOfPieces(new LinkedList<Piece>());
        SynchronousListOfPieces downloaded = new SynchronousListOfPieces(new LinkedList<Piece>());

        String remoteID = "-RU1103-<ÆD;»ÉÅµãûÌ";//"remote78901234567890";
        int piece_size = 32768; //780;

        PeerCommunicator peerComm1 =  new PeerCommunicator("local543210987654321", remoteID, socket1);
        PeerCommunicator peerComm2 =  new PeerCommunicator("local543210987654321", remoteID, socket2);
        PeerCommunicator peerComm3 =  new PeerCommunicator("local543210987654321", remoteID, socket3);
        peerComm1.piece_size = piece_size;
        peerComm2.piece_size = piece_size;
        peerComm3.piece_size = piece_size;

        int total_file_size = 14285814; //58887;
        String file_name = "downloaded.mov";


        // Download algorithm
        DownloadAlgorithmThread da_thread = new DownloadAlgorithmThread(
            new SimpleDownloadAlgorithm(total_file_size / peerComm1.piece_size),
            toDownload, peerComm1.piece_size, total_file_size
        );

        // model.FileSaver
        FileSaverThread fs_thread = new FileSaverThread(
            new FileSaver(file_name, total_file_size / peerComm1.piece_size + 1, downloaded, peerComm1.piece_size)
        );

        Thread algothread = new Thread(da_thread);
        Thread fsthread = new Thread(fs_thread);

        DownloadThread peer_thread1 = new DownloadThread(
            peerComm1, toDownload, downloaded, algothread, file_hash.getBytes(), total_file_size / piece_size + 1, piece_size
        );

        DownloadThread peer_thread2 = new DownloadThread(
            peerComm2, toDownload, downloaded, algothread, file_hash.getBytes(), total_file_size / piece_size + 1, piece_size
        );

        DownloadThread peer_thread3 = new DownloadThread(
            peerComm3, toDownload, downloaded, algothread, file_hash.getBytes(), total_file_size / piece_size + 1, piece_size
        );

        Thread peerthread1 = new Thread(peer_thread1);
        Thread peerthread2 = new Thread(peer_thread2);
        Thread peerthread3 = new Thread(peer_thread3);
        peerthread1.start();
        peerthread2.start();
        peerthread3.start();

        // Start peers first ('cause they have to handshake first)
        algothread.start();
        fsthread.start();

        algothread.join();
        System.out.println("Download algorithm completed");

        peerthread1.join();
        peerthread2.join();
        peerthread3.join();
        fsthread.join();
        System.out.println("model.FileSaver thread completed");

        System.out.println("Success !");
    }*/
}