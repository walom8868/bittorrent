import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.net.*;

import model.*;
import exceptions.*;
import utils.*;

public class Server {

    public static void main(String[] args) {
        new Server().startServer();

        boolean exit = false;
        while (!exit) {
            System.out.print("Continue? (y/n) ");
            Scanner reader = new Scanner(System.in);
            int input = reader.nextInt();
            if (input == 1) System.out.println("Exit !!");
        }

        return;
    }

    public void startServer() {
        final ExecutorService clientProcessingPool = Executors.newFixedThreadPool(10);

        Runnable serverTask = new Runnable() {
            @Override
            public void run() {
                try {
                    ServerSocket serverSocket = new ServerSocket(4444);
                    System.out.println("Waiting for clients to connect...");
                    while (true) {
                        Socket clientSocket = serverSocket.accept();
                        clientProcessingPool.submit(new ClientTask(clientSocket));
                    }
                } catch (IOException e) {
                    System.err.println("Unable to process client request");
                    e.printStackTrace();
                }
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();

    }

    private class ClientTask implements Runnable {
        private final Socket clientSocket;

        private ClientTask(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            System.out.println("Got a client !");

            try {
                // Do whatever required to process the client's request
                PrintWriter out =
                        new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));

                String inputLine, outputLine;
                while ((inputLine = in.readLine()) != null) {
                    //System.out.println("From client" + inputLine);
                    outputLine = "Back from server: " + inputLine;
                    out.println(outputLine);
                }
            }
            catch (IOException e) {
                System.out.println("NOOOOO");
            }

            /*try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }*/
        }
    }

}