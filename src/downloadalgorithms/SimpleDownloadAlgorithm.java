package downloadalgorithms;

public class SimpleDownloadAlgorithm implements DownloadAlgorithm {

	public int numberOfPieces;
	private int currentPiece = 0;

	public SimpleDownloadAlgorithm(int numberOfPieces) {
		this.numberOfPieces = numberOfPieces;
	}


	public int getNumberOfPieces() {
		return numberOfPieces;
	}

	public int getNextPiece() {
		// Always add an extra piece, because the last one will have different length than all the other ones.
		return (currentPiece <= numberOfPieces) ? currentPiece++ : -1;
	}

	// Nothing to do...
	public void init() {  }

	public static void main(String[] args) {
		DownloadAlgorithm algorithm = new SimpleDownloadAlgorithm(2);
		System.out.println(algorithm.getNextPiece());    // Should print 0
		System.out.println(algorithm.getNextPiece());	 // Should print 1
		System.out.println(algorithm.getNextPiece());    // Should print -1
	}
}