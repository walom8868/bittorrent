package downloadalgorithms;

public interface DownloadAlgorithm {

	/*
 	* This method returns the total number of pieces
 	*/
	int getNumberOfPieces();

	/*
	 * This method returns the next piece that should be downloaded as per the specific algorithm implementation
	 * @return int  Piece number, or -1 if all pieces have already been returned
	 */
	int getNextPiece();

	void init();
}