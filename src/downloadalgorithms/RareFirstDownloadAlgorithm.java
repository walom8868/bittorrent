package downloadalgorithms;

import model.Piece;
import utils.Utils;

import java.util.*;


public class RareFirstDownloadAlgorithm implements DownloadAlgorithm{

    // Set of bitsets to use
    private BitSet[] bitfields;

    // Mapping of Rareness-Piece
    private HashMap<String, LinkedList<String>> rarenessMapping;

    // Current position within the rareness mapping
    private int indexPosition;

    // Total number of pieces in the bitset
    private int numberOfPieces;

    // Waiting for bitfields to come ?
    private boolean waitingForBitfields = true;

    public RareFirstDownloadAlgorithm(BitSet[] bitfields, int numberOfPieces) {
        this.bitfields = bitfields;
        this.waitingForBitfields = false;
        this.numberOfPieces = numberOfPieces;
        this.rarenessMapping = new HashMap<>();
        this.indexPosition = 1;
    }

    public RareFirstDownloadAlgorithm(int numberOfPieces) {
        this.bitfields = null;
        this.numberOfPieces = numberOfPieces;
        this.rarenessMapping = new HashMap<>();
        this.indexPosition = 1;
    }

    public void setBitfields(BitSet[] bitfields) {
        this.bitfields = bitfields;
        this.waitingForBitfields = false;
    }

    public void init() {
        // Are we still waiting for the bitfields to come ?
        while (waitingForBitfields) {
            System.out.println("Waiting 3 seconds for bitfields to come...");
            try {
                Thread.sleep(3000);
            }
            catch (Exception e) {
                System.out.println("Something bad happened here...");
                e.printStackTrace();
            }
        }

        System.out.println("Bitfields ready, getting rareness mapping");

        int[] buffer = new int[this.numberOfPieces + 1];

        // Get # of bitfields and load all possible lists to use
        for (int i = 1; i <= bitfields.length; i++) {
            this.rarenessMapping.put(""+i, new LinkedList<String>());

            BitSet bitfield = this.bitfields[i-1];

            // Load pieces from this bitfield into buffer
            if (bitfield == null)
                continue;

            for(int j = 0; j <= this.numberOfPieces; j++) {
                // If this bitfield contains the piece, add it to the buffer
                // Last 4 pieces in the bitfields of the official -RU peers are bad, need to do a hack for it
                // Last pieces in bitfield show like this (Starting piece 430): 1100001111, and total # of pieces is 435.
                // Meaning that i need to skip 4 bits from the bitfield to get the right bits for the last 4 pieces.
                boolean badHackBecauseRUPeersBitfieldsAreBadAtTheEnd = true;
                if (j >= 432 && badHackBecauseRUPeersBitfieldsAreBadAtTheEnd)
                    j += 4;

                if (bitfield.get(j)) {
                    if (j >= 436 && badHackBecauseRUPeersBitfieldsAreBadAtTheEnd)
                        buffer[j-4] += 1;
                    else
                        buffer[j] += 1;
                }

                if (j >= 436 && badHackBecauseRUPeersBitfieldsAreBadAtTheEnd)
                    j -= 4;
            }
        }

        // Buffer contains rareness for all pieces, now load them into the Hashmap
        for (int i = 0; i <= this.numberOfPieces; i++) {
            List<String> list = this.rarenessMapping.get("" + buffer[i]);
            if (list == null) {
                System.out.println("LIST is NULL IN HERE : " + i + " - " + buffer[i]);
            }
            else {
                list.add("" + i);
            }
        }

        System.out.println("Rareness mapping complete");
    }

    public int getNumberOfPieces() {
        return this.numberOfPieces;
    }

    public int getNextPiece() {

        // Check that rareness exists within this mapping
        while (this.rarenessMapping.containsKey(""+this.indexPosition)) {

            // Take the next piece from the position we left off
            String next = this.rarenessMapping.get("" + this.indexPosition).poll();

            // If not null, returns an actual piece with this rareness
            if (next != null)
                return Integer.parseInt(next);

            // If it's null, go to next rareness level and check it exists in the mapping
            else
                this.indexPosition++;
        }

        // If reached this point, all pieces have been returned, so return -1
        return -1;
    }


    public static void main(String[] args) {
        // Byte arrays to use
        String  bytes1 = "10111000",
                bytes2 = "01001111",
                bytes3 = "10111110";

        int size = 8;

        // Test bitfields to use
        BitSet bitset1 = new BitSet(size);
        BitSet bitset2 = new BitSet(size);
        BitSet bitset3 = new BitSet(size);

        // Mark bits in each one
        Utils.bitSetFromString(bitset1, bytes1);
        Utils.bitSetFromString(bitset2, bytes2);
        Utils.bitSetFromString(bitset3, bytes3);

        // List to use, and bitsets loaded into it
        ArrayList<BitSet> list = new ArrayList<>();
        list.add(bitset1);
        list.add(bitset2);
        list.add(bitset3);

        //RareFirstDownloadAlgorithm algorithm = new RareFirstDownloadAlgorithm(list, size);

        int nextPiece;
        //while((nextPiece = algorithm.getNextPiece()) != -1) {
          //  System.out.println(nextPiece);
        //}
    }
}
