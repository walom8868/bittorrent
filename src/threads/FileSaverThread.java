package threads;

import model.FileSaver;

/**
 * Class to wrap model.FileSaver execution
 */
public class FileSaverThread implements Runnable {

    private FileSaver fs;

    public FileSaverThread(FileSaver fs) {
        this.fs = fs;
    }

    @Override
    public void run() {
        try {
            fs.saveFile();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
