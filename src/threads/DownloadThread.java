package threads;

import exceptions.PeerCommunicationException;
import exceptions.PeerException;
import exceptions.TrackerCommunicatorException;
import model.*;
import utils.Utils;

import java.io.EOFException;
import java.util.Arrays;
import java.util.BitSet;

/**
 * Created by roberto on 12/7/2015.
 */
public class DownloadThread implements Runnable {
    public PeerCommunicator peerComm = null;
    private final SynchronousListOfPieces toDownload;
    private final SynchronousListOfPieces downloaded;
    private final byte[] file_hash;
    private final int numberOfPieces;
    private final int piece_size;
    private final TrackerCommunicator.Peer peer;

    // Thread that puts pieces into "toDownload" queue
    // Used to check if this is still putting pieces in the queue, if not we can exit safely
    private final Thread inputThread;
    private Piece piece;
    private BitSet bitset;
    private String localID;

    // Bitset used to remember if a piece had a good or bad hash
    // If marked as 1, means we tried it already and has a bad hash
    // Else, if 0, we should attempt to download this piece
    private BitSet piecesBadHash = null;

    public DownloadThread(
            String localID, TrackerCommunicator.Peer peer, SynchronousListOfPieces toDownload,
            SynchronousListOfPieces downloaded, Thread inputThread,
            byte[] file_hash, int numberOfPieces, int piece_size) {
        this.localID = localID;
        this.peer = peer;
        this.toDownload = toDownload;
        this.downloaded = downloaded;
        this.inputThread = inputThread;
        this.file_hash = file_hash;
        this.numberOfPieces = numberOfPieces;
        this.piece_size = piece_size;

        this.piecesBadHash = new BitSet(numberOfPieces);
    }

    public DownloadThread(
            String localID, TrackerCommunicator.Peer peer, SynchronousListOfPieces toDownload,
            SynchronousListOfPieces downloaded, Thread inputThread,
            byte[] file_hash, int numberOfPieces, BitSet bitset, int piece_size) {
        this(localID, peer, toDownload, downloaded, inputThread, file_hash, numberOfPieces, piece_size);
        this.bitset = bitset;
    }

    @Override
    public void run() {
        try {
            this.peerComm = new PeerCommunicator(localID, peer.id, peer.ip, peer.port);
        }
        catch (Exception e) {
            debug("Unable to start communication with this peer. Error: " + e.getMessage());
        }

        boolean shouldTryToReconnect = false;
        try {
            debug("Starting thread to peer");

            // Perform handshake
            peerComm.initiateHandshake(file_hash);

            if (peerComm.getLocalBitfield() == null)
                peerComm.startBitfields(numberOfPieces);

            Message receivedMessage;
            boolean connectionReady = false;
            boolean bitfieldFirstAttempt = true;

            // Keep looping until list is empty
            // If more blocks are incoming, wait until they all are loaded
            while (toDownload.size() > 0 || inputThread.isAlive()) {

                if (!connectionReady) {
                    // Try to send the bitfield
                    try {
                        // If this is first attempt, send bitfield normally
                        if (bitfieldFirstAttempt) {
                            peerComm.sendBitfield(peerComm.getLocalBitfield(), numberOfPieces);
                            debug("1st attempt to send bitfield: normal way");
                        }
                        // Else, send alternate way
                        else {
                            peerComm.sendBitfieldAlternate(numberOfPieces);
                            debug("2nd attempt to send bitfield: alternative way");
                        }

                        // Try to read bitfield from the remote
                        receivedMessage = peerComm.receiveMessage();
                        MsgType type = MsgType.getID(receivedMessage.getType());

                        if (type == MsgType.BITFIELD) {
                            peerComm.setRemoteBitfield(receivedMessage);

                            // Send interested
                            peerComm.interested();

                            // Receive next message, should be unchoke
                            do {
                                receivedMessage = peerComm.receiveMessage();
                                type = MsgType.getID(receivedMessage.getType());
                                if (type == MsgType.UNCHOKE) {
                                    peerComm.setLocalChoked(false);
                                    debug("Connection ready!");
                                }
                                else
                                    debug("Received a message to ignore, ID: " + type.id());
                            } while (type != MsgType.UNCHOKE);
                        }

                        else if (type == MsgType.INTERESTED) {
                            peerComm.setRemoteInterested(true);

                            // Send unchoked
                            peerComm.unchoke();
                            debug("Remote is interested and unchoked. Connection ready");
                        }

                        connectionReady = true;
                        continue;
                    }
                    catch (EOFException e) {
                        debug("Lost connection with bitfield attempt 1, trying alternate way...");
                        bitfieldFirstAttempt = false;

                        // Reconnect to the peer
                        if (!peerComm.restartConnection()) {
                            debug("Unable to reconnect to peer.");
                            return;
                        }

                        // Start handshake again
                        peerComm.initiateHandshake(file_hash);

                        continue;
                    }
                }

                // If my interested message didn't receive an unchoke reply, just reply to messages from peer
                if (!peerComm.amIDownloadingFromPeer()) {
                    debug("--------------------- I'm not downloading from peer ----------------------");
                    return;
                }

                // If list is empty right now, wait a few milliseconds and try again
                if (toDownload.size() == 0) {
                    Thread.sleep(3000);
                    debug("Waiting 3 seconds for pieces from DownloadAlgorithm Thread");
                    continue;
                }

                // Pull the head of the list
                // Check if we should download the next piece
                boolean goodPieceToDownload = false;
                piece = toDownload.poll();
                int currentIndexTaken = 0;
                do {
                    if (this.piecesBadHash == null)
                        debug("badHash bitset is null");

                    if (piece == null)
                        debug("Piece is null!!");

                    // If this peer has a bad version of this piece (bad Hash verified), take next one
                    if (this.piecesBadHash.get(piece.index)) {
                        debug("Returning piece# " + piece.index + ", marked in badHash bitset");
                        piece = toDownload.addToHeadAndGetIndex(piece, currentIndexTaken + 1);
                    }
                    else
                        goodPieceToDownload = true;

                    // If piece is null, no more pieces to get, return...
                    if (piece == null) {
                        debug("Can't download more pieces from this peer, return...");
                        return;
                    }

                    currentIndexTaken++;
                } while (!goodPieceToDownload);

                debug("Pulled piece #" + piece.index);

                // Check that the peer has this piece
                // If it doesn't have it, put piece back in the head of the list and wait a second,
                // Piece will be eventually picked up by another peer
                int indexToCheck = piece.index;

                // BadHack again because of the official RUBT Clients sending bad bitfield in the last 4 bits
                if (new String(this.peer.id).contains("-RU") && indexToCheck >= 432)
                    indexToCheck += 4;

                if (!peerComm.getRemoteBitfield().get(indexToCheck)) {
                    debug("This peer doesn't have piece# " + piece.index + ". Waiting a second");
                    toDownload.addToHead(piece);
                    Thread.sleep(1000);
                    continue;
                }

                // Download the full piece
                if (downloadPiece(piece))
                    downloaded.add(piece);
                else {
                    // If download fails, re-add it to head of list
                    debug("Bad download, piece " + piece.index + " returned");
                    toDownload.addToHead(piece);
                }

                // To make things easier, send a keep alive after every piece
                //peerComm.sendKeepAlive();
            }
        }
        // If a PeerCommunication exception was thrown, simply notify user
        catch (EOFException|NullPointerException|PeerException|PeerCommunicationException e) {
            debug("Unable to start connection with peer. Error: " + e.getMessage());

        }
        // If the connection is lost, download might get stuck because of a missing piece
        catch (Exception e) {
            debug("Connection lost with a peer!  Error: " + e.getMessage());

            // Put the piece taken back into "toDownload" list
            if (piece != null) {
                debug("===========ADDING BACK TO HEAD");
                toDownload.addToHead(piece);
            }
        }

        finally {
            try {
                // Close connections
                peerComm.closeConnection();
            }
            // Nothing to do, if peerComm is null then connection already failed, so let it be
            catch (NullPointerException e) {
                ;
            }

            try {
                // Wait 2 seconds and try to reconnect if needed
                if (shouldTryToReconnect) {
                    Thread.sleep(2);
                    debug("Attempting to reconnect");
                    peerComm.openConnection();
                    run();
                }
            }
            catch (Exception e) {
                debug("Failed reconnection");
            }
        }
    }

    private boolean downloadPiece(Piece piece) throws Exception {
        int block_length;

        // Request blocks for this piece
        for (int i = 0; i < piece.blockNumber; i++) {

            // If this is the last piece, last block might have a different length
            if (piece.lastPiece && (i == piece.blockNumber - 1)) {
                block_length = piece.lastPieceLength % Block.BLOCK_LENGTH;
            }
            else {
                block_length = Block.BLOCK_LENGTH;
            }


            // Send Request
            debug("Request sent for block# " + i + " - length: " + block_length);
            peerComm.sendRequest(piece.index, i * Block.BLOCK_LENGTH, block_length);

            // Receive block
            Message pieceMessage = peerComm.receiveMessage();
            if (pieceMessage == null) {
                debug("Received null instead of piece's block");
            }
            Block block = pieceMessage.getBlock();

            debug(block.toString());

            // Add it to the piece
            piece.addBlock(block);
        }

        // Once piece is complete, verify it
        debug("Verifying hash piece# " + piece.index);
        byte[] actualHash = Utils.generateHash(piece.getData());


        if (!Arrays.equals(piece.expectedHash.array(), actualHash)) {
            debug("\n\n---------------Bad hash for piece# " + piece.index + "---------------------------\n");
            Thread.sleep(2000);

            // If this piece in this peer had a bad hash...
            // Mark it in our special bitset, we can still attempt other pieces, but not this one again
            this.piecesBadHash.set(piece.index);

            return false;
        }

        debug("Good hash piece# " + piece.index);

        peerComm.sendHave(piece.index);
        return true;
    }

    private void debug(String s) {
        try {
            System.out.println(peerComm.getRemoteIP() + ":  " + s);
        }
        catch (NullPointerException e) {
            System.out.println("Unable to start connection to one of the peers.");
        }
    }
}
