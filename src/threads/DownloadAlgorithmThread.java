package threads;

import downloadalgorithms.DownloadAlgorithm;
import model.Block;
import model.Piece;
import model.SynchronousListOfPieces;

import java.nio.ByteBuffer;


/**
 * Class to wrap download algorithm execution
 */
public class DownloadAlgorithmThread implements Runnable {

    private DownloadAlgorithm downloadAlgorithm;
    private SynchronousListOfPieces toDownload;

    // Details for the pieces
    private int piece_size, file_size;
    private ByteBuffer[] piece_hashes;

    public DownloadAlgorithmThread(
            DownloadAlgorithm downloadAlgorithm,
            SynchronousListOfPieces toDownload,
            int piece_size,
            int file_size,
            ByteBuffer[] piece_hashes) {
        this.downloadAlgorithm = downloadAlgorithm;
        this.toDownload = toDownload;
        this.piece_size = piece_size;
        this.file_size = file_size;
        this.piece_hashes = piece_hashes;
    }

    public DownloadAlgorithmThread(
            DownloadAlgorithm downloadAlgorithm,
            SynchronousListOfPieces toDownload,
            int piece_size,
            int file_size) {
        this.downloadAlgorithm = downloadAlgorithm;
        this.toDownload = toDownload;
        this.piece_size = piece_size;
        this.file_size = file_size;
    }


    @Override
    public void run() {
        // Init the algorithm
        this.downloadAlgorithm.init();

        // Load all pieces into the list
        int nextPiece;

        System.out.println("Start loading pieces");

        int total_pieces_added = 0;

        // End of pieces is marked as -1
        while ((nextPiece = downloadAlgorithm.getNextPiece()) != -1) {
            //System.out.println("nextPiece: " + nextPiece);
            Piece piece = new Piece(nextPiece, piece_size / Block.BLOCK_LENGTH, this.piece_hashes[nextPiece]);

            // Last piece? (Remember the additional piece added in download algorithm)
            if (nextPiece == downloadAlgorithm.getNumberOfPieces()
                    && file_size % piece_size != 0) {
                piece.isLastPiece(file_size % piece_size);
            }

            // Add to queue
            toDownload.add(piece);

            // Update total # of pieces
            total_pieces_added = nextPiece;
        }

        // Show only how many pieces were added in total
        System.out.println("Total number of pieces in queue: " + total_pieces_added);
    }
}
