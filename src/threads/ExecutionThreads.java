package threads;

import downloadalgorithms.DownloadAlgorithm;
import downloadalgorithms.SimpleDownloadAlgorithm;
import model.*;
import utils.*;

import java.util.*;
import java.nio.*;

public class ExecutionThreads {

    public static void main(String[] args) throws Exception {
    	SynchronousListOfPieces toDownload = new SynchronousListOfPieces(new LinkedList<Piece>());

    	DownloadAlgorithmThread da_thread = new DownloadAlgorithmThread(
    		new SimpleDownloadAlgorithm(5), toDownload, 3, 25);

    	Thread thread = new Thread(da_thread);
        thread.start();
    	thread.join();

    	System.out.println(toDownload);


        Piece piece0 = new Piece(0, new byte[] {'a', 'b', 'c', 'd', 'e'});
        Piece piece1 = new Piece(1, new byte[] {'0', '1', '2', '3', '4'});
        final Piece piece2 = new Piece(2, new byte[] {'f', 'g', 'h', 'i', 'j'});
        Piece piece3 = new Piece(3, new byte[] {'5', '6', '7', '8', '9'});
        Piece piece4 = new Piece(4, new byte[] {'z', 'y', 'x', 'w', 'v'});

        final SynchronousListOfPieces list = new SynchronousListOfPieces(new LinkedList<Piece>());

        list.add(piece0);
        list.add(piece4);
        list.add(piece3);
        list.add(piece1);
        //list.add(piece2);

        int numberofpieces = 5;

        FileSaver fs = new FileSaver("testsave.txt", numberofpieces, list, 5);

        new Thread(new FileSaverThread(fs)).start();

        Runnable delayInput = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    list.add(piece2);
                }
                catch (Exception e) {
                    System.out.println("bad");
                }
            }
        };

        (new Thread(delayInput)).start();
    }
}